# Base on Ruby 2.3.x, debian:jessie based.
FROM ruby:2.3
ARG BOSH=2.0.48
ARG CF=6.34.1
ENV LANG=C.UTF-8
ENV HOME=/tmp
# build-essential, golang-go: for bosh-google-cpi plugin that BOSH v2 CLI pulls
# unzip, curl, jq, python-openssl, openssh: for accessing GCP
# git, s3cmd, netcat-openbsd: tests & distribution
RUN apt-get update -y && apt-get install -y \
    git-core git s3cmd unzip curl jq python-openssl \
    netcat-openbsd openssh-client \
    apt-transport-https ca-certificates \
    build-essential golang-go ;

# install gcloud sdk
RUN echo "deb https://packages.cloud.google.com/apt cloud-sdk-xenial main" \
        | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list ;\
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - ;\
    apt-get update -y && apt-get install -y google-cloud-sdk && apt-get clean -y

# install BOSH CLI v2 & Cloud Foundry CLI
# Intentionally using pre-compiled, non-packaged Go binaries from upstream project!
RUN curl -LJO https://s3.amazonaws.com/bosh-cli-artifacts/bosh-cli-${BOSH}-linux-amd64 ;\
    mv bosh-cli-${BOSH}-linux-amd64 /usr/bin/bosh ;\
    chmod +x /usr/bin/bosh ;\
    curl -LJO https://s3-us-west-1.amazonaws.com/cf-cli-releases/releases/v${CF}/cf-cli_${CF}_linux_x86-64.tgz ;\
    tar xzvf cf-cli_${CF}_linux_x86-64.tgz ;\
    rm cf-cli_${CF}_linux_x86-64.tgz ;\
    chmod +x cf && mv cf /usr/bin/cf ;




