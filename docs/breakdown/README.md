# Breakdown of components

This document is intended to describe what makes up a BOSH release, and how the GitLab EE BOSH Release is put together.

Some attempts at definition here are a little counter intuitive, due to the documentation from BOSH being similarily confusing. For clarity, see https://bosh.io/docs

## Packages (`packages/`)
https://bosh.io/docs/packages.html

#### gitlab-ee-omnibus

This provides the Omnibus GitLab, scripts for initialization, and templates for various configurations.

#### debian_nfs_server

This provides the contents of Ubuntu (Trusty 14.04) package `nfs-kernel-server_1.2.5-3ubuntu3_amd64.deb`, which provide the NFS server, kernel modules, etc.

#### go

Go (1.4.1) will be unpacked and used for the compilation of `route-registrar`

#### route-registrar

[route-registrar](https://github.com/cloudfoundry/route-registrar) is a Go program that is used to interact with the Cloud Foundry [gorouter](https://github.com/cloudfoundry/gorouter). This is how we register names with the external load balancing services so that `https://gitlab.mycloudfoundry.tld` is routed to a given job.

Our included version may be out of date, but continues to function well at this time.

## Job Templates (`jobs/`)

[Jobs Templates](https://bosh.io/docs/jobs.html) represent chunks of work that a release performs.

#### file_server
This job template provides the NFS file services that will act as the shared storage for deployed gitlab-ee-omnibus job(s).

#### nfs_mounter
This job template provides scripts and service files for the mounting, unmounting, and monitoring state of NFS mounts that will be used.

#### gitlab-ee

This job template contains the actual GitLab EE Omnibus package that will provide HTTP(S), Git, SSH, etc. At this time, we do not separate any of the Omnibus packaged services into different jobs.

These instances (default 1) will register with the

## Jobs
[Jobs](http://bosh.io/docs/reference/jobs.html) are a realization of packages, i.e. running one or more processes from a package. A job contains the configuration files and startup scripts to run the binaries from a package.

A job typically includes:

* metadata that specifies available configuration options
ERB configuration files
* a Monit file that describes how to start, stop and monitor processes
* start and stop scripts for each process
* additional hook scripts

#### file_server

The file_server [job][#jobs] consumes the `file_server` job template.
* There will be only one NFS server that any number of `gitlab-ee` jobs will connect to in order to share filesystems.
* This [job][#jobs] will not be started if an external NFS server is provided in the configuration properties.

#### gitlab-ee

This [job][#jobs] contains the actual GitLab EE Omnibus package that will provide HTTP(S), Git, SSH, etc. At this time, we do not separate any of the Omnibus packaged services into different jobs.

These instances (default 1) will consume:
* `route-registrar` job template, linking external URLs to the deployed `gitlab-ee-omnibus`.
* `nfs_mounter` job template, mounting the NFS share from the `file_server` job into the instance.
* `gitlab-ee-omnibus` job template, providing (currently) all services as a part of the Omnibus package.

## Unsupported features
Currently, there are a few features of a normal Omnibus GitLab installation that
are not present as a part of this packaging system. To find them see the [technical debt & feature](https://gitlab.com/gitlab-pivotal/p-gitlab-ee/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=feature&label_name[]=technical%20debt) labels on [p-gitlab-ee](https://gitlab.com/gitlab-pivotal/p-gitlab-ee)
