
echo "Configuring BOSH Director connection"

# Set the use of SHA2 checksums
export BOSH_SHA2=1
# Set bosh environment
export BOSH_ENVIRONMENT=gcp
export BOSH_CLIENT=admin
export BOSH_CLIENT_SECRET=`bosh int bosh/creds.yml --path /admin_password`
export common_password=$BOSH_CLIENT_SECRET

export bosh_ip=$(gcloud compute ssh ubuntu@bosh-bastion --ssh-key-file=gcloud.key --ssh-flag="-n" -- host $(jq -r .current_vm_cid bosh/state.json)|cut -d" " -f4)

bosh -e $bosh_ip --ca-cert <(bosh int bosh/creds.yml --path /director_ssl/ca) alias-env gcp
