echo "Setting up SSH Dynamic Proxy to GCP"

gcloud config set compute/region $(jq -r .region.value tfstate.json)
gcloud config set compute/zone $(jq -r .zone.value tfstate.json)
gcloud compute ssh ubuntu@bosh-bastion \
    --ssh-key-file=`pwd`/gcloud.key --force-key-file-overwrite \
    --ssh-flag="-D2525" --ssh-flag="-f" --ssh-flag="-N" --ssh-flag="-4"
export BOSH_ALL_PROXY=socks5://localhost:2525
