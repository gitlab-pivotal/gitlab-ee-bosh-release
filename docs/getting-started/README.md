# Setup your machine to work with GitLab BOSH

After you've cloned this repository, you will need access to multiple systems in order to start working on GitLab BOSH release.

## Prerequisites

* Access to `Build` vault
* Access to [gitlab-pivotal][gitlab-pivotal] group on GitLab.com
* Access to [gitlab-pivotal][gce] project on Google Cloud Engine
* [gcp-infra][gcp-infra] must be activated.
* This repo up to date
  * If you don't have this repository locally: clone with submodules - `git clone --recursive git@gitlab.com:gitlab-pivotal/gitlab-ee-bosh-release.git`
  * If you have this repository locally: fetch the latest version - `git pull origin master && git pull --recurse-submodules`

## Setup

### Setup .env file

1. In your local clone, check out to a new branch: `git checkout -b release-X.Y.Z`, where X.Y.Z is GitLab EE version.
1. Copy [`.env_example`](.env_example) to `.env`
1. Edit `.env` and complete the following values:
  1. *From `Build` vault, search gcp-infra:*. Copy the `*_KEY` value and place it between single quotes (`''`)
  1. *From `Build` vault, search CF AWS values*. Add them corresponding AWS values
  1. *From `Build` vault, search EE test license*. Add to `GITLAB_LICENSE`.
1. Other values should not be changed under normal circumstances.
  1.  If you change `BOSH_RELEASE_NAME`, `OLD_RELEASE_VERSION` will need to be appropriate updated in order to handle `rspec` tests.
  1. `BOSH_DEPLOYMENT_NAME` is *intentionally* made to result in a value akin to `gitlab-ee-developername` in order to all parallel development. This value is used extensively to maintain separation of resources, and can be viewed as a type of namespace.
  1.  Only change the `BOSH_MANIFEST` value if you have strong need, and remember that any changes should be migrated into the original copy.

### Setup container

This project has a container for building, and you can use this for your own development work.

1. Confirm that you are located in the local clone of this repository
1. Pull the container
  `docker pull registry.gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release`
1. Run the container, mounting in your clone.
  1. *Linux:* `docker run -ti -v $(pwd):/bosh:rw  -v /etc/passwd:/etc/passwd:ro --env HOME=/tmp --name bosh-release -u $(id -u):$(id -g) registry.gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release /bin/bash -l`
  1. *OS X with native Docker:* `docker run -it -v $(pwd):/bosh:rw -v ~/.config:/.config:rw --name bosh-release -u root:root registry.gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release /bin/bash -l`
1. Change to the mapped directory inside the container.
  `cd /bosh`
1. Activate the environment you created in [Setup .env file](#setup-env-file), via `source .env`

#### OS X note

Depending on your setup, you might need to do also create a mount for a `passwd` and `group` files. This can be useful in case you need to debug a failed deploy using
`bosh ssh -d gitlab-ee-master gitlab-ee/0`. In this case you could consider starting a
container and copying `/etc/passwd` and `/etc/group` contents over to a local directory
you can use for mounting.

You might also need to mount a `.config` directory so that `gcloud` commands can write to it.

In these cases, running container might look something like:

```
docker run -it -v $(pwd):/bosh:rw -v ~/.config:/.config:rw -v ~/LOCAL_PATH/TO/passwd:/etc/passwd:rw -v ~/LOCAL_PATH/TO/group:/etc/group:rw --name bosh-release -u root:root registry.gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release /bin/bash -l
```

After you've completed these steps, you can go back to [Developing against GCP](https://gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release/tree/master#developing-against-gcp).

[bosh]: https://bosh.io "BOSH"
[gcp-infra]: https://gitlab.com/gitlab-pivotal/gcp-infra
[cf-cli]: https://docs.cloudfoundry.org/cf-cli/ "cf-cli"
[gitlab-pivotal]: https://gitlab.com/gitlab-pivotal
[gce]: https://console.cloud.google.com/home/dashboard?project=gitlab-pivotal-160416
