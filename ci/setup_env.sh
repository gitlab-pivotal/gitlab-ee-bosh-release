# Run with: . ci/setup_env.sh
echo $GCP_TERRAFORM_KEY > terraform.key.json
gcloud auth activate-service-account --key-file=terraform.key.json
gcloud config set project $(jq -r .project_id terraform.key.json)
# Pull metedata from the GCP project
ci/gcloud_metadata.rb -f=tfstate.json,bosh/creds.yml,bosh/state.json,cloudfoundry/creds.yml,gcloud.key,gcloud.key.pub
chmod 600 ./gcloud.key
# Connect to the bastion host
. ci/gcloud_ssh_proxy.sh
# Configure BOSH environment
. ci/bosh_configure.sh
# Generate the config/private.yml with AWS keys
ci/generate_private_yml
