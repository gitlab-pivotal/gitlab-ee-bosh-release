# clean up the CF credentials we created before.
# this assumes that `cf login` is still in place from previous steps.
ci/cf_target.sh

# delete mysql service-key, service
cf delete-service-key mysql db -f || true
cf delete-service mysql -f || true

# delete redis service-key, service
cf delete-service-key redis db -f || true
cf delete-service redis -f || true

cf delete-space $BOSH_DEPLOYMENT_NAME -f || true
