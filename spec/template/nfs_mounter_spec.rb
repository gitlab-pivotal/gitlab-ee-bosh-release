require 'spec_helper'
require 'bosh/template/test'
require 'yaml'

describe 'nfs_mounter templates' do

  let(:manifest) { YAML.load_file(bosh_manifest_path) }
  let(:release_path) { File.join(File.dirname(__FILE__), '../..') }
  let(:release) { Bosh::Template::Test::ReleaseDir.new(release_path) }
  let(:job) { release.job('nfs_mounter') }
  let(:properties) { manifest['instance_groups'].find { |instance| instance['name'] == "gitlab-ee" }['properties'] }

  before do
    properties['gitlab-ee']['nfs'] = {'share' => '/'}
  end

  context 'handle_nfs_blobstore' do

    let(:template) { job.template('bin/handle_nfs_blobstore.sh') }

    context 'when BOSH Link is configured for NFS' do
      let(:links) do
        [
          Bosh::Template::Test::Link.new(
            name: 'gitlab_nfs',
            instances: [Bosh::Template::Test::LinkInstance.new(address: 'nfs.gitlab')]
          )
        ]
      end
      let(:rendered) { template.render(properties, consumes: links) }

      context 'nfs server address is NOT provided' do
        it 'configures using address from Link' do
          expect(rendered).to include("nfs.gitlab:/")
        end
      end

      context 'nfs server address is provided' do
        before do
          properties['gitlab-ee']['nfs']['address'] = 'nfs.outside'
        end

        it 'configures using address from property' do
          expect(rendered).to include("nfs.outside:/")
        end
      end
    end

    context 'when BOSH Link is NOT configured for NFS' do
      context 'nfs server address is NOT provided' do
        it 'raise error' do
          expect{
            template.render(properties)
          }.to raise_error "No NFS server was provided, by BOSH Link or address."
        end
      end

      context 'nfs server address is provided' do

        let(:rendered) { template.render(properties) }

        before do
          properties['gitlab-ee']['nfs'] = {
            'address' => 'example.org',
            'share' => '/home/user/nfs'
          }
        end

        it 'configures using provided property' do
          expect(rendered).to include('example.org:/home/user/nfs')
        end
      end
    end
  end

  context 'nfs_mounter_ctl' do

    let(:template) { job.template('bin/nfs_mounter_ctl') }
    let(:rendered) { template.render(properties) }

    context 'when mount point is configured' do
      before do
        properties['gitlab-ee']['nfs']['mount_point'] = '/home/user/nfs_mount'
      end

      it 'configures using provided value' do
        expect(rendered).to include('export NFS_MOUNT_POINT=/home/user/nfs_mount')
      end
    end

    context 'when mount point is NOT configured' do

      it 'configures using default value' do
        expect(rendered).to include('export NFS_MOUNT_POINT=/var/vcap/nfs')
      end
    end
  end

end
