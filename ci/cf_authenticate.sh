# Authenticate to deployed Cloud Foundry
# ** run from base of checkout

set -e

# login
bosh int cloudfoundry/creds.yml --path /cf_admin_password | \
    cf login --skip-ssl-validation \
        -a https://api.$(jq -r .cf_ip.value tfstate.json).nip.io -u admin
