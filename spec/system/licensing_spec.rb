require 'spec_helper'

describe 'licensed gitlab-ee' do

  before do
    setup_gitlab_license
  end

  after do
    remove_gitlab_license
  end

  it 'has a license' do
    license = rails_runner_cmd(first_node_ip, "puts License.current.data")
    expect(license).to eq(gitlab_license)
  end
end
