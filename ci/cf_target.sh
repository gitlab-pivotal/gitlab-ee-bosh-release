
set -e

# create org (if not exist)
cf create-org gitlab
cf target -o gitlab
# create space (if not exist)
cf create-space $BOSH_DEPLOYMENT_NAME
cf target -o gitlab -s $BOSH_DEPLOYMENT_NAME
