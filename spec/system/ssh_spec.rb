require 'spec_helper'

#TODO these tests assume a running instance
describe "GitLab SSH functionality" do
  before(:all) do
    setup_gitlab_license
    @key = generate_key
    @key_id = add_key(ssh_public_key(@key), "test")
  end

  after(:all) do
    delete_key(@key_id)
    remove_gitlab_license
  end

  it 'can connect to SSH', retry: 5 do
     expect {
        Net::SSH.start(
            first_node_ip,
            "git",
            port: gitlab_ee_ssh_port,
            key_data: [private_key_pem(@key)].compact,
            proxy: connect_to_proxy
        )
      }.to_not raise_error
  end

  describe 'pushing a new repository via SSH' do
    before(:all) do
      @test_project_id = create_project
    end

    after(:all) do
      delete_project(@test_project_id)
    end
  # This test is the reason we need netcat in the docker container. Debian Jessie doesn't have OpenSSH with -J flag, so we need to use the tunnel.
    around(:each) do |example|
      Dir.mktmpdir do |dir|
          %x{
            cd #{dir}

            echo #{private_key_pem(@key).shellescape} > private_key
            chmod 0600 private_key

            echo 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o  ProxyCommand="nc -x #{connect_to_proxy.proxy_host}:#{connect_to_proxy.proxy_port} %h %p" -i private_key $*' > ssh
            chmod +x ssh
            export GIT_SSH='./ssh'

            git init
            git remote add origin #{gitlab_ee_git_ssh_url(test_project_name,gitlab_ee_ssh_port)}
            touch README.md
            git add README.md
            git commit -m "add README"
            git push -u origin master
          }
        
        example.run
      end
    end

    it 'exists on the server' do
      forward_to_node(first_node_ip, gitlab_ee_port) do |port|
        branches = gitlab_ee_api_get(port, api_key, "projects/#{@test_project_id}/repository/branches")

        expect(branches.count).to eq(1)
        expect(branches.first["name"]).to eq("master")
      end
    end
  end
end
