require 'spec_helper'

describe "Core GitLab EE functionality" do

  before(:all) do
    setup_gitlab_license
    @initial_project_count = project_count
    @test_project_id = create_project
  end

  after(:all) do
    delete_project(@test_project_id)
    remove_gitlab_license
  end

  it 'has a project' do
    expect(project_count).to eq(@initial_project_count + 1)
  end

  it 'has the data' do
    expect_project_data
  end

  it 'accessible externally via HTTPS URL' do
    expect {
      RestClient::Request.execute(
        method: :get,
        url: gitlab_ee_external_sign_in_url('https'),
        verify_ssl: false,
        max_redirects: 0,
      )
    }.to_not raise_error
  end

  it 'redirects HTTP to HTTPS' do
    expect {
      RestClient::Request.execute(
        method: :get,
        url: gitlab_ee_external_sign_in_url('http'),
        verify_ssl: false,
        max_redirects: 0,
      )
    }.to raise_error(/Maximum number of redirect reached/)
  end

  describe 'pushing a new repository via HTTP' do
    around(:each) do |example|
      Dir.mktmpdir do |dir|
        %x{
          cd #{dir}
          git init
          git remote add origin #{gitlab_ee_git_http_url(test_project_name)}
          touch README.md
          git add README.md
          git commit -m "add README"
          git -c http.sslVerify=false push -u origin master
        }
        example.run
      end
    end

    it 'exists on the server' do
      forward_to_node(first_node_ip, gitlab_ee_port) do |port|
        branches = gitlab_ee_api_get(port, api_key, "projects/#{@test_project_id}/repository/branches")

        expect(branches.count).to eq(1)
        expect(branches.first["name"]).to eq("master")
      end
    end
  end
end
