require 'net/ssh/gateway'
require 'net/ssh/proxy/socks5'
require 'rest-client'
require 'yaml'
require 'json'
require 'securerandom'
require 'digest'
require 'base64'

module SystemHelper
  # Using a class variable, so that we maintain a single Net::SSH::Gateway/Proxy
  # These being class items reduces the number of actively opened connections.
  @@ssh_gateway = nil
  @@ssh_proxy = nil

  # Check if a file exists on a given node, via `ls` over ssh with +exec_on_node+
  def file_exists?(node_ip, path)
    result = exec_on_node(node_ip, "ls #{path}", root: true)
    return result.lines.last == "#{path}\n"
  end

  def exec_bosh command
    output = `bosh -d #{deployment_name} -n #{command}`
    expect($?.to_i).to eq(0), output
  end

  def first_node_ip
    node_ips[0]
  end

  def second_node_ip
    node_ips[1]
  end

  def node_ips
    ips = []
    # get the output of bosh, in JSON
    instances = `bosh -d #{deployment_name} instances --json`
    nodes = JSON.load(instances)
    nodes['Tables'][0]['Rows'].each do |vm|
      if vm['instance'].start_with?('gitlab-ee')
        ips.push(vm['ips'].split(',')[0])
      end
    end
    ips.sort!
  end

  # Create a dynamic tunnel to the environment, via the bastion host.
  def connect_to_gateway
    if @@ssh_gateway.nil?
      @@ssh_gateway = Net::SSH::Gateway.new(
        bastion_host,
        bastion_ssh_user,
        keys: [bastion_ssh_private_key].compact,
      )
    end
    @@ssh_gateway
  end

  # Provide access to the existing SOCKS5 proxy through the bastion host.
  # This is the proxy already provided by `ci/gcloud_ssh_proxy.sh` and
  # used by `ci/bosh_configure.sh`, and all bosh commands.
  def connect_to_proxy
    if @@ssh_proxy.nil?
      @@ssh_proxy = Net::SSH::Proxy::SOCKS5.new('127.0.0.1',2525)
    end
    @@ssh_proxy
  end

# Create an SSH port forward from localhost to instance +node_ip+, targetting port +port+.
# This will make use of +connect_to_gateway+ creating a SSH proxy tunnel to reach
# the target node directly. Intended for use where destination is +localhost+,
# but not required.
  def forward_to_node(node_ip, port)
    connect_to_gateway.open(node_ip, port) do |forward_port|
      yield forward_port
    end
  end

# Execute command +cmd+ on +node+ over SSH, using +options+
# ==== Options:
# :user #=> 'vcap'
# :root #=> false
# :keys #=> [bastion_ssh_private_key]
  def exec_on_node(node, cmd, options = {})
    user           = options.fetch(:user, 'vcap')
    run_as_root    = options.fetch(:root, false)
    keys = options.fetch(:keys, [bastion_ssh_private_key])
    discard_stderr = options.fetch(:discard_stderr, false)

    if run_as_root
      cmd = "sudo -S bash -c #{cmd.shellescape}"
    end

    cmd << ' 2>/dev/null' if discard_stderr

    # run command over ssh, using the proxy to allow direct connection
    Net::SSH.start(
      node,
      user,
      keys: keys.compact,
      paranoid: false,
      proxy: connect_to_proxy
    ) do |ssh|
      ssh.exec!(cmd)
    end
  end

  def rails_runner_cmd(node, cmd)
    cmd = "/opt/gitlab/bin/gitlab-rails runner #{cmd.shellescape}"
    exec_on_node(node, cmd, root: true).lines.last.chomp
  end

  def deployment_name
    ENV['BOSH_DEPLOYMENT_NAME'] || 'gitlab-ee'
  end

  def release_name
    ENV['BOSH_RELEASE_NAME'] || 'gitlab-ee'
  end

  def bosh_manifest
    file_path = "downloaded-manifest.yml"
    unless @manifest_downloaded
      exec_bosh "manifest > #{file_path}"
      @manifest_downloaded = true
    end
    file_path
  end

  def bosh_manifest_path
    ENV['BOSH_RELEASE_MANIFEST'] || 'manifests/gitlab-ee-bosh-gcp.yml'
  end

# 'Old' manifest, with former properties/jobs/etc.
  def old_bosh_manifest_path
    ENV['BOSH_OLD_RELEASE_MANIFEST'] || "manifests/gitlab-ee-bosh-gcp-old.yml"
  end

## recent: post GitLab 8.13, see http://docs.pivotal.io/partners/gitlab/release.html
## https://gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release/pipelines/5434060
## - Tile v 1.1.3 has BOSH gitlab-ee-127.tgz has GitLab EE 8.14.5
  def old_release_version
    ENV['OLD_RELEASE_VERSION'] || '127'
  end

  def bosh_manifest_yaml
    @bosh_manifest_yaml ||= YAML.load_file(bosh_manifest)
  end

  def temp_bosh_manifest_path
    "temp-manifest.yml"
  end

  def bastion_host
    ENV['BASTION_HOST'] || `jq -r .bastion_ip.value tfstate.json`.chomp!
  end

  def bastion_ssh_user
    ENV['BASTION_SSH_USER'] ||'ubuntu'
  end

  def bastion_ssh_private_key
    ENV['BASTION_SSH_PRIVATE_KEY'] || 'gcloud.key'
  end
end
