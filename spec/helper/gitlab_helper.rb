require 'securerandom'

module GitlabHelper

  def check_gitlab_node_connectivity(node_ip)
    forward_to_node(node_ip, gitlab_ee_port) do |port|
      RestClient::Request.execute(
        :method => :get ,
        :url => gitlab_ee_login_url(port) ,
        :verify_ssl => false,
        :max_redirects => 0,
      )
    end
  end

  def setup_gitlab_license
    remove_gitlab_license

    rails_runner_cmd(first_node_ip, "License.create(data: '#{gitlab_license}')")
  end

  def remove_gitlab_license
    rails_runner_cmd(first_node_ip, "License.destroy_all")
  end

  def api_key
    @api_key ||= rails_runner_cmd(
      first_node_ip,
      "puts User.first.try(:private_token) || PersonalAccessToken.create(name: '#{SecureRandom.hex(2)}', user: User.first, expires_at: 5.days.from_now, scopes: ['api']).token"
    )
  end

  def test_project_name
    @test_project_name ||= "awesome-#{SecureRandom.uuid}"
  end

  def create_project
    test_project_id = nil
    # add the git data
    forward_to_node(first_node_ip, gitlab_ee_port) do |port|
      #setup project
      json_body = gitlab_ee_api_post(port, api_key, "projects", name: test_project_name)
      test_project_id = json_body['id']
    end

    test_project_id
  end

  def delete_project(id)
    forward_to_node(first_node_ip, gitlab_ee_port) do |port|
      #setup project
      gitlab_ee_api_delete(port, api_key, "projects/#{id}")
    end
  end

  def add_key(key, title)
    key_id = nil
    forward_to_node(first_node_ip, gitlab_ee_port) do |port|
      json_body = gitlab_ee_api_post(port, api_key, "user/keys", key: key, title: title)
      key_id = json_body['id']
    end
    key_id
  end

  def delete_key(id)
    forward_to_node(first_node_ip, gitlab_ee_port) do |port|
      gitlab_ee_api_delete(port, api_key, "user/keys/#{id}")
    end
  end

  def project_count
    forward_to_node(first_node_ip, gitlab_ee_port) do |port|
      gitlab_ee_api_get(port, api_key, "projects/all").count
    end
  end

  def expect_project_data(node_ip = first_node_ip)
    cmd = "ls /var/vcap/nfs/shared/git-data/repositories/root/#{test_project_name}.git"
    result = exec_on_node(node_ip, cmd, root: true)
    expect(result).to include("HEAD")
  end

  def gitlab_ee_port
    ENV['GITLAB_EE_PORT'] || 80
  end

  def gitlab_ee_ssh_port
    ENV['GITLAB_EE_SSH_PORT'] || 2222
  end

  def gitlab_ee_url port
    "http://127.0.0.1:#{port}"
  end

  def gitlab_ee_login_url port
   "#{gitlab_ee_url(port)}/users/sign_in"
  end

  def gitlab_ee_api_url port
   "#{gitlab_ee_url(port)}/api/v3"
  end

  def gitlab_ee_api_delete(port, api_key, endpoint, params = {})
    url = "#{gitlab_ee_api_url(port)}/#{endpoint}"
    RestClient.delete(url, { private_token: api_key }.merge(params))
  end

  def gitlab_ee_api_post(port, api_key, endpoint, params = {})
    url = "#{gitlab_ee_api_url(port)}/#{endpoint}"
    JSON.parse(RestClient.post(url, { private_token: api_key }.merge(params)))
  end

  def gitlab_ee_api_get(port, api_key, endpoint, params = {})
    url = "#{gitlab_ee_api_url(port)}/#{endpoint}"
    JSON.parse(RestClient.get(url, { private_token: api_key }.merge(params)))
  end

  def gitlab_ee_job_path
    "/var/vcap/jobs/gitlab-ee"
  end

  def gitlab_ee_sys_logs
    "/var/vcap/sys/log/gitlab"
  end

  def gitlab_ee_store_path
    "/var/vcap/nfs/shared"
  end

  def gitlab_ee_log log_file
    File.join(gitlab_ee_sys_logs, log_file)
  end

  def gitlab_license
    @gitlab_license ||= ENV['GITLAB_LICENSE'].gsub("\n", "")
  end

  def gitlab_ee_git_http_url(project_name)
    url = "#{gitlab_ee_external_url}/root/#{project_name}.git"
    url.sub!('https://', ['https://root:', gitlab_ee_root_password, '@'].join(''))
    url
  end

  def gitlab_ee_git_ssh_url(project_name, port)
    "ssh://git@#{first_node_ip}:#{port}/root/#{project_name}.git"
  end

  def gitlab_ee_external_url(proto = 'https')
    properties = bosh_manifest_yaml['instance_groups'].find { |instance| instance["name"] == "gitlab-ee" }['properties']
    "#{proto}://#{properties['gitlab-ee']['route_name']}.#{properties['cf']['apps_domain']}"
  end

  def gitlab_ee_external_sign_in_url(proto = 'https')
    "#{gitlab_ee_external_url(proto)}/users/sign_in"
  end

  def gitlab_ee_root_password
    YAML.load_file('bosh/creds.yml')['admin_password'] || "5iveL!fe"
  end
end
