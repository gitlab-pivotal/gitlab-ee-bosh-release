deployment_name=$1
deployment_name=${deployment_name:-gitlab-ee}
release_name=$2
release_name=${release_name:-gitlab-ee}
version=$3
version=${version:-latest}

bosh -n interpolate \
    -v deployment_name=${deployment_name} \
    -v release_name=${release_name} \
    -v version=${version} \
    -v zone=$(jq -r .zone.value tfstate.json) \
    -v network=gitlab \
    -v private_cidr=$(jq -r .private_cidr.value tfstate.json) \
    -v root_domain=$(jq -r .cf_ip.value tfstate.json).nip.io \
    -v mysql_host=$(jq -r .hostname mysql.json) \
    -v mysql_port=$(jq -r .port mysql.json) \
    -v mysql_name=$(jq -r .name mysql.json) \
    -v mysql_username=$(jq -r .username mysql.json) \
    -v mysql_password=$(jq -r .password mysql.json) \
    -v redis_host=$(jq -r .host redis.json) \
    -v redis_port=$(jq -r .port redis.json) \
    -v redis_password=$(jq -r .password redis.json) \
    -v nats_password=$(bosh int cloudfoundry/creds.yml --path /nats_password) \
    -v root_password=${common_password} \
    -v default_az=z1 \
    -v stemcell_version=latest \
    $BOSH_MANIFEST
