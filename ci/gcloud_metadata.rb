#!/usr/bin/env ruby

# Short gcloud metadata key extraction script, so we have a reliable cache
# Uses Zlib to compress and Base64 to encode.
# Limits: 32K/item, 512K in total

# -f = fetch: comma seperated list of filenames
# -d = delete: comma seperated list of filenames
# -a = add/update: comma seperated list of filenames

require 'yaml'
require 'zlib'
require 'base64'
require 'tempfile'

PROJECT_FILES = [
  'terraform/terraform.tfstate',
  'tfstate.json',
  'bosh/creds.yml',
  'bosh/state.json',
  'cloudfoundry/creds.yml',
  'gcloud.key',
  'gcloud.key.pub',
].freeze

# Fetch the metadat for the project via gcloud, extract a subset, return a hashmap
def fetch_gcloud_metadata(files = [])
  output={}

  if files.any?
    metadata=`gcloud compute project-info describe`
    metadata=YAML.load(metadata)
    metadata['commonInstanceMetadata']['items'].each do |item|
      if files.include? field_to_filename(item['key'])
        output[field_to_filename(item['key'])] = item['value']
      end
    end
  end

  output
end

# Store the metadata in the project.
# file = path to file, will be turned into an acceptable field name
def store_gcloud_metadata(filename)
  r = false

  if File.readable?(filename)
    temp = Tempfile.new()
    # read in all of the content
    File.open(filename) do |f|
      #encode & write to tempfile
      temp.write(encode_metadata(f.read))
      temp.close
    end
    #call metadata-from-file on tempfile (can't provide large cmdline input)
    field = filename_to_field(filename)
    r = system("gcloud compute project-info add-metadata --metadata-from-file=#{field}=#{temp.path}")
    #remove tmpfile
    temp.unlink
  else
    puts "store: not readable: #{filename}"
  end

  r
end

# Remove from metadata in project
# file = path to file, will be turned into acceptable field name
def delete_gcloud_metadata(filename)
  field = filename_to_field(filename)
  system("gcloud compute project-info remove-metadata --keys=#{field}")
end

# Encode the metadata (deflate, encode)
def encode_metadata(data)
  deflated = Zlib::deflate(data)
  encoded = Base64.encode64(deflated)
end

# Decode the metadate (decode, inflate)
def decode_metadata(data)
  decoded = Base64.decode64(data)
  inflated = Zlib::inflate(decoded)
end


def field_to_filename(field)
  field.tr('-','/').tr!('_','.')
end

def filename_to_field(filename)
  filename.tr('/','-').tr!('.','_')
end

def fetch_arguments(delimiter)
  arguments = []
  match = "-#{delimiter}="

  # find our selected arguments
  subset = ARGV.select { |c| c.start_with?(match) }

  subset.each do |item|
    list = item.sub(match,'').split(',')
    list.select! { |i| PROJECT_FILES.include? i }
    list.uniq!
    arguments.concat(list)
  end

  arguments
end

files = {
  'add' => [],
  'delete' => [],
  'fetch' => []
}

if ARGV.any?
  fetch_arguments('a').each do |f|
    if store_gcloud_metadata(f)
      puts "Stored metadata for #{f}"
    else
      puts "FAILED to store metadata for #{f}"
      exit 1
    end
  end

  fetch_arguments('d').each do |f|
    if delete_gcloud_metadata(f)
      puts "Removed metadata for #{f}"
    else
      puts "FAILED to remove metadata for #{f}"
      exit 1
    end
  end

  data = fetch_gcloud_metadata(fetch_arguments('f'))
  data.each do |filename,content|
    puts "Fetched #{filename}"

    if not Dir.exist?(File.dirname(filename))
      Dir.mkdir(File.dirname(filename))
    end

    File.open(filename,'w') do |f|
      f.write(decode_metadata(content))
    end
  end

else
  puts "No parameters passed."
  exit 3
end
