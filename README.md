[gcp-infra]: https://gitlab.com/gitlab-pivotal/gcp-infra

## Setup your machine

To get access to the required machines in order to be able to do a release,
follow the [Getting Started Guide](docs/getting-started/README.md).

## Developing against GCP

This project uses the same BOSH micro as CI, however thanks to namespacing via `$BOSH_DEPLOYMENT_NAME`, collisions are avoided. The dev release name is `gitlab-ee-dev`. The MySQL and Redis services used are provided by brokers in the Cloud Foundry instance as a part of the [gcp-infra][gcp-infra] project.

The pre-configured manifests can be found in [manifests/gitlab-ee-bosh-gcp.yml](manifests/gitlab-ee-bosh-gcp.yml).

The scripts needed for all access and resources curation are _all_ provided as part of the scripts present in [`ci/`](ci/). Their usage is in between comments in the source and in [`.gitlab-ci.yml`](.gitlab-ci.yml).
These are currently summarized and extracted from `.gitlab-ci.yml`'s `before_script:` as [ci/setup_env.sh](ci/setup_env.sh).

*After reading the above*, verify that you are inside of the container and run:

```
. ci/setup_env.sh
```

*It is important that you use `.` or `source`, as you need the environment that this script will export!*

Once you ran this script, you should get output with `Succeeded` note.


## Upgrading GitLab EE version

_If at any time you have questions, please use the `create_release:` and `deploy:` stages in `.gitlab-ci.yml` as a reference for any missing commands for handling the deployment._

1. Ensure you have followed [Setup your machine](#setup-your-machine), so that
your environment and variables are configured. It is expected that you already ran through [Developing against GCP](#developing-against-gcp) to complete the
configuration and appropriate connections.

1.  Update/Upgrade the included package with script that will fetch it from the package server:
```sh
ci/package_release 8.14.5-ee.0
```

1. Edit [`config/blobs.yml`](config/blobs.yml) and remove the block with previous version under `gitlab-ee-omnibus/gitlab-ee_***amd64.deb.`

1. Sync blobs and submodules
```sh
BOSH_ALL_PROXY= bosh -n sync-blobs
git submodule init && git submodule update
```

1. Build the BOSH release and upload
```sh
bosh clean-up
bosh create-release --name $BOSH_RELEASE_NAME --force
bosh upload-release
```
  *Note:* if you encounter errors due to blobs, remove `.dev_builds` directory and try again.

1. Generate Cloud Foundry services for MySQL and Redis:
```sh
ci/cf_authenticate.sh
ci/cf_cleanup_credentials.sh
ci/cf_generate_credentials.sh
```
These will produce `mysql.json` and `redis.json` that are consumed by the deployment.
1. Deploy your release
```sh
ci/bosh_deploy.sh $BOSH_DEPLOYMENT_NAME $BOSH_RELEASE_NAME $(ci/read_latest_version "$BOSH_RELEASE_NAME")
```

1. Prepare the environment for running tests
```sh
git config --global user.email "ci@tile.gitlab.com"
git config --global user.name "CI"
bundle install
```

1.  Run the product tests
```sh
bundle exec rake spec:product
```

1. Reset the Cloud Foundry credentials
```sh
ci/cf_cleanup_credentials.sh
ci/cf_generate_credentials.sh
```

1. Ensure `OLD_RELEASE_VERSION` is present on BOSH Director
```sh
bosh -n upload-release https://s3-eu-west-1.amazonaws.com/${S3_RELEASE_URL/s3:\/\//}/releases/${BOSH_RELEASE_NAME}/${BOSH_RELEASE_NAME}-${OLD_RELEASE_VERSION/+/%2B}.tgz --name=${BOSH_RELEASE_NAME} --version=${OLD_RELEASE_VERSION}
```

1. Run the upgrade tests
```sh
bundle exec rake spec:upgrades
```

1. Upload the blob
```sh
bosh upload-blobs
```

1. Commit your changes.
```sh
git add config/blobs.yml
git add packages/gitlab-ee-omnibus/packaging
git add packages/gitlab-ee-omnibus/spec
git commit
```

1. Push your branch

1. Confirm CI passes

1. Create a merge request against master

1. Once the merge request has been approved, a pipeline will be started. The final task is `create_final_version`. Once that is complete, look for the the line `Upload bosh release version XXX, from master SHA:yyyyyyyyyyyyyyyyyyyyyyyyy`. Make a note of the release version.

1. Cleanup your environment. Remove the deployment and Cloud Foundry credentials.
```sh
bosh -d $BOSH_DEPLOYMENT_NAME delete-deployment --force
ci/cf_cleanup_credentials.sh
```

### Running Tests Locally Against GCP

See [`.gitlab-ci.yml`](.gitlab-ci.yml) `deploy` stage for available sub-sections of tests as used in CI.

```
bundle exec rspec --format=documentation
```

### Running Template Tests

```
bundle exec rspec --format=documentation spec/template/gitlab.rb.erb spec/template/sshd_config.erb
```
