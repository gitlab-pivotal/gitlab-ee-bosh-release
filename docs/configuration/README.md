# Configuration of BOSH

Configuration of a [BOSH][bosh] deployed instance of [Omnibus-GitLab][omnibus-gitlab] is done through a deployment manifest file, as present in [`manifests`](../../manifests).  This document is intended to provide an overview of the key parts of the configuration as required for development.

To configure your environment for BOSH and related CI scripts, see [Getting Started](../getting-started/README.md)

# Configuring a Deployment

The use of the [BOSH cli v2][bosh] allows for variable interpolation in manifest YAML files. This project is designed around the [infrastructure][gcp-infra] built on Google Cloud Platform.

Barring adding features, there should not be any requirements for editing of the provided [`manifests/gitlab-ee-bosh-gcp.yml`](../../manifests/gitlab-ee-bosh-gcp.yml). Scripts are provided under [ci/](../../ci/) to enable access and configuration to use the BOSH director provided by [gcp-infra][gcp-infra]. You should only have to run [bosh][bosh] and [cf][cf-cli] commands by hand for debugging problems.


## Configuring the Release

The `name` property defines the name of the deployment, and must be unique. `version` will only need to be edited away from `latest` when it is necessary to test a previous version of a bosh release tarball.

The above values will be handled by scripts in [`ci/`](../../ci/) and will be based on the environment.

## Configuring the Resource Pools

The `resource_pools` property is used to define the instance size and resource allocations for the VM instances that will be created on a `bosh deploy` call. It is suggested for performance reasons that the `gitlab-ee` instance be of `n1-standard-2`, for the sake of defined disk, and CPU performance. The NFS, `file_server` will need little storage, and is recommended to be only an `gq-small` and have 1 GB of disk.

These recommendations are already in place, and should _only_ be altered under necessity or testing explicitly for resource contention / performance. Never commit such changes without updating this documentation.

[bosh]: https://bosh.io "BOSH"
[gcp-infra]: https://gitlab.com/gitlab-pivotal/gcp-infra
[cf-cli]: https://docs.cloudfoundry.org/cf-cli/ "cf-cli"
[omnibus-gitlab]: https://gitlab.com/gitlab-org/omnibus-gitlab "Omnibus-GitLab"
[build-team]: https://about.gitlab.com/handbook/build/ "Build Team"
