require 'spec_helper'
require 'bosh/template/test'
require 'yaml'

describe 'sshd_config Template' do

  let(:manifest) { YAML.load_file(bosh_manifest_path) }
  let(:release_path) { File.join(File.dirname(__FILE__), '../..') }
  let(:release) { Bosh::Template::Test::ReleaseDir.new(release_path) }
  let(:job) { release.job('gitlab-ee') }
  let(:properties) { manifest['instance_groups'].find { |instance| instance['name'] == "gitlab-ee" }['properties'] }

  before do
    properties['gitlab-ee']['ssh']['port'] = 2222
    properties['gitlab-ee']['ssh']['extraConfig'] = ''
  end

  describe 'SSH Daemon Configuration' do

    let(:template) { job.template('config/sshd_config') }
    let(:rendered) { template.render(properties) }

    it 'templates Port' do
      expect(rendered).to include("Port 2222")
    end

    context 'when extraConfig is a single line' do
      before do

        properties['gitlab-ee']['ssh']['extraConfig'] = "Ciphers favorite-is-steak"
      end

      it "templates a single entry" do
        expect(rendered).to end_with("Ciphers favorite-is-steak\n")
      end
    end

    context 'when extraConfig is multiple lines' do
      before do

        properties['gitlab-ee']['ssh']['extraConfig'] = "Ciphers favorite-is-steak\nMACs are-expensive"
      end

      it "templates multiple lines" do
        expect(rendered).to end_with("Ciphers favorite-is-steak\nMACs are-expensive\n")
      end
    end

    context 'when extraConfig is blank' do
      before do

        properties['gitlab-ee']['ssh']['extraConfig'] = ""
      end

      it "templates a blank line" do
        expect(rendered).to end_with("\n\n")
      end
    end
  end
end
