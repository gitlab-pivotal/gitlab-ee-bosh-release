require 'spec_helper'

#TODO these tests assume a running instance
describe 'VM recreation' do

  before(:all) do
    setup_gitlab_license
  end

  after(:all) do
    remove_gitlab_license
  end

  context 'a single node GitLab deployment with git data' do

    before(:all) do
      @test_project_id = create_project
    end

    after(:all) do
      delete_project(@test_project_id)
    end

    context 'when the NFS server is recreated' do

      before(:all) do
        exec_bosh "recreate --force file_server/0"
      end

      describe 'gitlab' do
        it 'is still accessible' do
          check_gitlab_node_connectivity(first_node_ip)
        end

        it 'still has the git data', retry: 10 do
          # it can take a few moments to remount nfs share
          expect_project_data
        end
      end
    end

    context 'when GitLab is recreated' do
      before(:all) do
        exec_bosh "recreate --force gitlab-ee/0"
      end

      describe 'gitlab' do
        it 'is still accessible' do
          check_gitlab_node_connectivity(first_node_ip)
        end

        it 'still has the git data' do
          expect_project_data
        end
      end
    end
  end
end

