require 'openssl'

module SshHelper

  def generate_key
    OpenSSL::PKey::RSA.new(2048)
  end

  def private_key_pem(key)
    key.to_pem
  end

  def ssh_public_key(key)
    public_key = key.public_key
    type = public_key.ssh_type
    data = [ public_key.to_blob ].pack('m0')
    "#{type} #{data}"
  end

end
