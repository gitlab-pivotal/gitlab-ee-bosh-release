require 'spec_helper'

#TODO these tests assume a running instance
#TODO these tests assume both releases are on bosh director
describe 'product upgrades', upgrades: true do

  before(:all) do
    # read in the 'old' manifest, with interpolation
    bosh_manifest_temp = YAML.load(`BOSH_MANIFEST=#{old_bosh_manifest_path} ci/bosh_interpolate.sh #{deployment_name} #{release_name} #{old_release_version}`)

    #write the updated config
    File.open(temp_bosh_manifest_path, "w") { |file| file.write(YAML.dump(bosh_manifest_temp)) }

    exec_bosh "deploy  #{temp_bosh_manifest_path}"

    setup_gitlab_license
  end

  after(:all) do
    remove_gitlab_license

    exec_bosh "delete-deployment"
  end

  context 'GitLab deployment with git data' do

    before(:all) do
      @test_project_id = create_project
    end

    after(:all) do
      delete_project(@test_project_id)
    end

    context 'when upgrade performed' do

      before(:all) do
        exec_bosh "deploy #{bosh_manifest}"
      end

      describe 'gitlab' do
        it 'is still accessible' do
          check_gitlab_node_connectivity(first_node_ip)
        end

        it 'still has the git data' do
          expect_project_data
        end

        it 'has abuse reports' do
          expect(rails_runner_cmd(first_node_ip, "puts AbuseReport.table_exists? rescue puts false")).to eq "true"
        end
      end
    end
  end
end

