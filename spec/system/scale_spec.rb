require 'spec_helper'

#TODO these tests assume a running instance
describe 'GitLab EE scales' do

  before(:all) do
    setup_gitlab_license
  end

  after(:all) do
    remove_gitlab_license
  end

  context 'when a node has git data' do

    before(:all) do
      @test_project_id = create_project
    end

    after(:all) do
      delete_project(@test_project_id)
    end

    context 'when the deployment scale from one node to two and has a smaller persistant disk' do

      before(:all) do
        bosh_manifest_temp = YAML.load_file(bosh_manifest)

        # update the manifest to have 2 gitlab-ee nodes
        gitlab_ee_instance = bosh_manifest_temp['instance_groups'].find { |instance| instance["name"] == "gitlab-ee" }
        gitlab_ee_instance['instances'] = 2

        # update the manifest to smaller disk size
        @disk_type = '1GB'
        @disk_size = 1000
        file_server_instance = bosh_manifest_temp['instance_groups'].find { |instance| instance["name"] == "file_server" }
        file_server_instance['persistent_disk_type'] = @disk_type

        #write the updated config
        File.open(temp_bosh_manifest_path, "w") { |file| file.write(YAML.dump(bosh_manifest_temp)) }
        exec_bosh "deploy #{temp_bosh_manifest_path}"
      end

      after(:all)  do
        exec_bosh "deploy  #{bosh_manifest}"
      end

      describe 'accesing the git data' do
        it 'is available on the first node' do
          expect_project_data(first_node_ip)
        end

        it 'is available on the second node' do
          expect_project_data(second_node_ip)
        end
      end

      it 'has correct disk space on nfs' do
        result = exec_on_node(first_node_ip, "df -BM --output=size /var/vcap/nfs", root: true).lines.last.chomp.to_i
        expect(result).to be_within(@disk_size*0.05).of(@disk_size)
      end
    end
  end
end
