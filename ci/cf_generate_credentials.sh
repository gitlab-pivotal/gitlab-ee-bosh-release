# Create & Authenticate to Cloud Foundry
# ** run from base of checkout.

set -e

. ci/cf_target.sh

# create service for mysql (if not exist)
services=$(cf services | grep '^p-mysql' | cut -d' ' -f1)
if [ "$services" != "p-mysql" ]; then
  cf create-service p-mysql 100mb mysql
fi
# delete existing service key, if it exists
cf delete-service-key mysql db -f || true
# create service-key for mysql
cf create-service-key mysql db
cf service-key mysql db | grep -v Getting > mysql.json
echo "MySQL DB: $(jq -r .name mysql.json)"

# create service for redis (if not exist)
services=$(cf services | grep '^p-redis' | cut -d' ' -f1)
if [ "$services" != "p-redis" ]; then
  cf create-service p-redis shared-vm redis
fi
# delete existing service key, if it exists
cf delete-service-key redis db -f || true
# create service-key for redis
cf create-service-key redis db
cf service-key redis db | grep -v Getting > redis.json
echo "Redis DB: $(jq -r .host redis.json):$(jq -r .port redis.json)"
