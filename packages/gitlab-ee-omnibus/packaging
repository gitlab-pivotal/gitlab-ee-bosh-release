# abort script on any command that exits with a non zero value
set -xe
set -o pipefail

# extract debian archive and then
dpkg -x gitlab-ee-omnibus/gitlab-ee_10.4.4-ee.0_amd64.deb "${BOSH_INSTALL_TARGET}"

# this patch is required for gitlab-shell to properly create authorized_keys
# otherwise the authorized_keys gitlab-shell command points to /var/vcap/data/packages/gitlab-ee-omnibus/...,
# because the File.expand_path is used to resolve the gitlab-shell directory
# this happens, because we use /opt/gitlab symlink to /var/vcap/packages/gitlab-ee-omnibus
patch -d "${BOSH_INSTALL_TARGET}/opt/gitlab" -p0 < gitlab-ee-omnibus/fix_gitlab_shell_path.patch

# Add `ar_innodb_row_format` monkey patch, to ensure new InnoDB tables use Barracuda/DYNAMIC storage.
# See https://gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release/issues/15
patch -d "${BOSH_INSTALL_TARGET}/opt/gitlab" -p0 < gitlab-ee-omnibus/add_ar_innodb_row_format_initializer.patch

RUNIT_RECIPE="${BOSH_INSTALL_TARGET}/opt/gitlab/embedded/cookbooks/runit/recipes/default.rb"
if [[ ! -e $RUNIT_RECIPE ]]; then
  echo "WARNING: Missing file $RUNIT_RECIPE."
  exit 1
fi

echo > $RUNIT_RECIPE
