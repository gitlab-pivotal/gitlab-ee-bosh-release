require 'spec_helper'
require 'bosh/template/test'
require 'yaml'

describe 'Templates' do

  let(:manifest) { YAML.load_file(bosh_manifest_path) }
  let(:release_path) { File.join(File.dirname(__FILE__), '../..') }
  let(:release) { Bosh::Template::Test::ReleaseDir.new(release_path) }
  let(:job) { release.job('gitlab-ee') }
  let(:properties) { manifest['instance_groups'].find { |instance| instance['name'] == "gitlab-ee" }['properties'] }

  before do
    properties['redis']['host'] = "host"
    properties['redis']['port'] = 5589

    properties['database']['name'] = "gitlab-ee"
    properties['database']['encoding'] = "utf8mb4"
    properties['database']['host'] = "host"
    properties['database']['port'] = 36000
    properties['database']['username'] = "mysql-user"
    properties['database']['password'] = "mysql-pass"
    properties['database']['adapter'] = "mysql2"

    properties['gitlab-ee']['email_from'] = 'tile@pivotal.io'
    properties['gitlab-ee']['nfs'] = {'mount_point' => '/var/vcap/nfs'}

    properties['gitlab-ee']['initial_root_password'] = 'initial'

    properties['smtp']['address'] = 'tile.smtp'
    properties['smtp']['port'] = 1234

    properties['smtp']['authentication'] = 'login'
    properties['smtp']['tls'] = 'true'
    properties['ldap']['enabled'] = 'false'

    properties['nginx']['trusted_addresses'] = []
    properties['nginx']['pcf_trusted_addresses'] = []

    properties['rackattack']['enabled'] = 'true'

    #puts properties.to_yaml
  end

  describe 'GitLab Omnibus File' do

    let(:template) { job.template('config/gitlab.rb') }
    let(:rendered) { template.render(properties) }

    it 'templates the redis hostname' do
      expect(rendered).to include("gitlab_rails['redis_host'] = \"host\"")
    end

    context 'when the initial root password is set' do
      before do
        properties['gitlab-ee']['initial_root_password'] = "initial"
      end

      it "templates the value into the password" do
        expect(rendered).to include("gitlab_rails['initial_root_password'] = \"initial\"")
      end
    end

    context 'when the username and password are set for redis' do

      before do
        properties['redis']['password'] = "pass"
      end

      it "templates the value into the password" do
        expect(rendered).to include("gitlab_rails['redis_password'] = \"pass\"")
      end
    end

    it 'templates the GitLab email settings' do
      expect(rendered).to include("gitlab_rails['gitlab_email_from'] = 'tile@pivotal.io'")
    end

    context 'when email display name and reply to is set' do
      before do
        properties['gitlab-ee']['email_display_name'] = 'Pivotal Tile'
        properties['gitlab-ee']['email_reply_to'] = 'tile@pivotal.io'
      end

      it 'templates the GitLab email display name' do
        expect(rendered).to include("gitlab_rails['gitlab_email_display_name'] = 'Pivotal Tile'")
        expect(rendered).to include("gitlab_rails['gitlab_email_reply_to'] = 'tile@pivotal.io'")
      end
    end

    it 'templates the SMTP settings' do
      expect(rendered).to include("gitlab_rails['smtp_address'] = \"tile.smtp\"")
      expect(rendered).to include("gitlab_rails['smtp_port'] = 1234")
      expect(rendered).to include("gitlab_rails['smtp_authentication'] = \"login\"")
      expect(rendered).to include("gitlab_rails['smtp_tls'] = true")
    end

    context 'when user_name, password & domain are provided' do
      before do
        properties['smtp']['user_name'] = 'smtp_tile'
        properties['smtp']['password'] = 'password'
        properties['smtp']['domain'] = 'smtp.domain'
      end

      it 'templates these settings' do
        expect(rendered).to include("gitlab_rails['smtp_user_name'] = \"smtp_tile\"")
        expect(rendered).to include("gitlab_rails['smtp_password'] = \"password\"")
        expect(rendered).to include("gitlab_rails['smtp_domain'] = \"smtp.domain\"")
      end

    end

    context 'when LDAP is enabled, and servers array is provided' do
      before do
        properties['ldap']['enabled'] = 'true'
        properties['ldap']['servers'] = [{"label" => 1}, {"label" => 2}]
      end

      it 'templates the LDAP servers array' do
        expect(rendered).to match(/^gitlab_rails\['ldap_enabled'\] = true/)
        expect(rendered).to match(/^gitlab_rails\['ldap_servers'\]/)
      end
    end

    context 'when LDAP is enabled, but server array is empty' do
      before do
        properties['ldap']['enabled'] = 'true'
        properties['ldap']['servers'] = []
      end

      it 'templates LDAP as disabled, as have must servers configured' do
        expect(rendered).not_to match(/^gitlab_rails\['ldap_enabled'\] = true/)
        expect(rendered).not_to match(/^gitlab_rails\['ldap_servers'\]/)
      end
    end

    context 'when LDAP is disabled, nothing is set.' do
      before do
        properties['ldap']['enabled'] = 'false'
        properties['ldap']['servers'] = []
      end

      it 'templates LDAP as disabled, as have must servers configured' do
        expect(rendered).not_to match(/^gitlab_rails\['ldap_enabled'\] = true/)
        expect(rendered).not_to match(/^gitlab_rails\['ldap_servers'\]/)
      end
    end

    context 'when NGINX is provided neither trusted_addresses or pcf_trusted_addresses ' do
      it 'does not template NGINX real_ip_trusted_addresses' do
        expect(rendered).not_to match(/^nginx\['real_ip_trusted_addresses'\]/)
      end
    end

    context 'when NGINX is provided trusted_addresses but NOT pcf_trusted_addresses' do
      before do
        properties['nginx']['trusted_addresses'] = ['trusted_addresses']
      end

      it 'templates NGINX real_ip_trusted_addresses as trusted_addresses' do
        expect(rendered).to match(/^nginx\['real_ip_trusted_addresses'\] = \["trusted_addresses"\]/)
      end
    end

    context 'when NGINX is provided pcf_trusted_addresses but NOT trusted_addresses' do
      before do
        properties['nginx']['pcf_trusted_addresses'] = ['pcf_trusted_addresses']
      end

      it 'templates NGINX real_ip_trusted_addresses as pcf_trusted_address' do
        expect(rendered).to match(/^nginx\['real_ip_trusted_addresses'\] = \["pcf_trusted_addresses"\]/)
      end
    end

    context 'when NGINX is provided pcf_trusted_addresses and trusted_addresses' do
      before do
        properties['nginx']['trusted_addresses'] = ['trusted_addresses']
        properties['nginx']['pcf_trusted_addresses'] = ['pcf_trusted_addresses']
      end

      it 'templates NGINX real_ip_trusted_addresses as trusted_addresses and pcf_trusted_addresses' do
        expect(rendered).to match(/^nginx\['real_ip_trusted_addresses'\] = \["trusted_addresses", "pcf_trusted_addresses"\]/)
      end
    end

    context 'when RackAttack is enabled' do
      it 'templates RackAttack as enabled' do
        expect(rendered).to match(/^gitlab_rails\['rack_attack_git_basic_auth'\]\s+=\s+{\s+'enabled'\s=>\strue,/m)
      end
    end

    context 'when RackAttack is disabled' do
      before do
        gitlab_ee_instance = manifest['instance_groups'].find { |instance| instance['name'] == "gitlab-ee" }
        properties['rackattack']['enabled'] = 'false'
      end

      it 'templates RackAttack as disabled' do
        expect(rendered).to match(/^gitlab_rails\['rack_attack_git_basic_auth'\]\s+=\s+{\s+'enabled'\s=>\sfalse,/m)
      end
    end

    context 'when database encoding is "utf8"' do
      before do
        gitlab_ee_instance = manifest['instance_groups'].find { |instance| instance['name'] == "gitlab-ee" }
        properties['database']['encoding'] = "utf8"
      end

      it 'templates encoding as "utf8mb4"' do
        expect(rendered).to include("gitlab_rails['db_encoding'] = \"utf8mb4\"")
      end

      it 'templates collation as "utf8mb4_general_ci"' do
        expect(rendered).to include("gitlab_rails['db_collation'] = \"utf8mb4_general_ci\"")
      end
    end
  end

  describe 'GitLab Initialize File' do

    let(:template) { job.template('bin/gitlab-ee-initialize') }
    let(:rendered) { template.render(properties) }

    it 'templates the mysql connection details' do
      expect(rendered).to include("\"--host=host\" \"--port=36000\" \"--user=mysql-user\" \"--password=mysql-pass\" -e 'CREATE DATABASE IF NOT EXISTS `gitlab-ee` CHARACTER SET utf8mb4 collate utf8mb4_general_ci'")
    end
  end
end
