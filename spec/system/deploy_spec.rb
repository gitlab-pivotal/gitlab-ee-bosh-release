require 'spec_helper'
require 'net/ssh/gateway'
require 'rest-client'
require 'yaml'
require 'json'
require 'securerandom'
require 'digest'

describe 'GitLab EE' do
  let (:initialized) { File.join(gitlab_ee_job_path, "initialized") }

  describe 'Initial Checks' do
    it 'is available by the IP address' do
      check_gitlab_node_connectivity(first_node_ip)
    end

    describe 'initial configuration' do
      it 'creates gitlab configuration' do
        expect(file_exists?(first_node_ip, initialized)).to be_truthy
      end

      it 'initializes database' do
        expect(file_exists?(first_node_ip, File.join(gitlab_ee_store_path, "db_initialized"))).to be_truthy
      end
    end

    describe 'process management' do
      it "ensures that omnibus doesn't create upstart job" do
        expect(file_exists?(first_node_ip, "/etc/init/gitlab-runsvdir.conf")).to be_falsy
      end
    end

    describe 'logs dir' do
      let(:log_files) {
        [
          'gitlab-ee-initialize.log',
          'gitlab-ee-runsvdir.log',
          'gitlab-ee-db-setup.log',
          'gitlab-rails/production.log',
          'gitlab-rails/sidekiq.log',
          'nginx/gitlab_error.log',
          'nginx/gitlab_access.log',
          'nginx/error.log',
          'unicorn/unicorn_stderr.log',
          'unicorn/unicorn_stdout.log'
        ]
      }

      it 'should have all gitlab_ee related logs' do
        log_files.each do |log_file|
          log_path = gitlab_ee_log log_file
          expect(file_exists?(first_node_ip, log_path)).to be_truthy
        end
      end
    end
  end
end
